package com.technodyne.automation.service;

import com.technodyne.automation.helper.DatabaseType;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

/**
 * interface to define database services
 * author @Supun
 * Since 16/09/2018
 */
public interface IDatabaseService {

	int getCount(String query, Map<Integer,Object> paramsMap,DatabaseType dbType, String dbName);

	ResultSet execute(String query,Map<Integer,Object> paramsMap, DatabaseType dbType, String dbName);

	Map<String, Object> getOneRowData(String query,Map<Integer,Object> paramsMap, DatabaseType dbType, String dbName);

	List<Object> getListData(String query,Map<Integer,Object> paramsMap, DatabaseType dbType, String dbName);
}
