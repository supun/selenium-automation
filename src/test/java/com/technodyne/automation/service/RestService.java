package com.technodyne.automation.service;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service public class RestService implements IRestService {
	private Logger logger = Logger.getLogger(this.getClass());

	@Override public HttpResponse get(String url) {
		HttpResponse response = null;
		logger.info("getting data from: " + url);
		try {
			HttpGet get = new HttpGet(url);
			HttpClient httpClient = HttpClientBuilder.create().build();
			response = httpClient.execute(get);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return response;
	}

	@Override public HttpResponse postData(String json, String url) {
		HttpResponse response = null;
		logger.info("posting data to: " + url);
		try {
			HttpPost post = new HttpPost(url);
			HttpClient httpClient = HttpClientBuilder.create().build();
			StringEntity postingString = new StringEntity(json);
			post.setHeader("Content-type", "application/json");
			post.setEntity(postingString);
			response = httpClient.execute(post);
		} catch (IOException e) {
			logger.error(e);
			e.printStackTrace();
		}
		return response;
	}

	@Override public HttpResponse postFormData(Map<String, Object> contentMap, String url) {
		HttpResponse response = null;

		logger.info("posting form data to: " + url);
		try {
			HttpPost post = new HttpPost(url);
			HttpClient httpClient = HttpClientBuilder.create().build();
			post.setHeader("Content-type", "application/x-www-form-urlencoded");
			post.setEntity(createFormEntity(contentMap));
			response = httpClient.execute(post);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}

		return response;
	}

	private HttpEntity createFormEntity(Map<String, Object> contentMap) throws UnsupportedEncodingException {
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();

		// set form data
		for (String key : contentMap.keySet()) {
			String value = String.valueOf(contentMap.get(key));
			nvps.add(new BasicNameValuePair(key, value));
		}

		return new UrlEncodedFormEntity(nvps);
	}
}
