package com.technodyne.automation.service;

import org.apache.http.HttpResponse;

import java.util.Map;

public interface IRestService {

	HttpResponse get(String url);

	HttpResponse postData(String json, String url);

	HttpResponse postFormData(Map<String, Object> contentMap, String url);
}
