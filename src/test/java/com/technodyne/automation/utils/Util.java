package com.technodyne.automation.utils;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class Util {

	private static Logger logger = Logger.getLogger(Util.class);

	public static String encodeValue(String value) {
		String encoded = null;
		try {
			encoded = URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException e) {
			logger.error("Error encoding parameter {}", e);
		}
		return encoded;
	}

	public static String decode(String value) {
		String decoded = null;
		try {
			decoded = URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException e) {
			logger.error("Error encoding parameter {}", e);

		}
		return decoded;
	}

	public static <T> T parseJson(String s, Class<T> type) {
		Gson g = new Gson();
		return (T) g.fromJson(s, type);
	}

	public static String stringify(Object json) {
		Gson g = new Gson();
		return g.toJson(json);
	}

}
