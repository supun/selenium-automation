package com.technodyne.automation.utils;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

// utility class to handle / extract data httpResponse
public class RetrieveUtil {
	public static <T> T retrieveResourceFromResponse(HttpResponse response, Class<T> clazz) throws IOException {

		String jsonFromResponse = EntityUtils.toString(response.getEntity());
		return Util.parseJson(jsonFromResponse, clazz);
	}
}
