package com.technodyne.automation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration @ComponentScan(basePackages = { "com.technodyne.automation" }) @PropertySources({
		@PropertySource(value = "classpath:integration.properties")}) public class AppConfig {

	// this is must to resolve ${} in @Value
	@Bean public static PropertySourcesPlaceholderConfigurer propertyConfig() {
		return new PropertySourcesPlaceholderConfigurer();
	}

}
