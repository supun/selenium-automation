package com.technodyne.automation.helper;

/**
 * db_type ENUM to handle multiple db connections
 * author @Supun
 * Since 16/09/2018
 */
public enum DatabaseType {
	DB_MYSQL, DB_POSTGRESQL;
}
