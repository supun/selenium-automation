package com.technodyne.automation.testng;

import com.technodyne.automation.AppConfig;
import com.technodyne.automation.selenium.config.SeleniumConfig;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

@ContextConfiguration(classes = AppConfig.class) public class SeleniumTestNGTest extends AbstractTestNGSpringContextTests
{
    @Value("${yas_result_page_url}") private String resultsPageUrl;
    private SeleniumConfig config;

    @BeforeSuite public void setUp()
    {
        config = new SeleniumConfig();
    }

    @AfterSuite public void tearDown()
    {
        config.close();
    }

    @Test public void whenResultPageLoaded() throws InterruptedException
    {
        config.navigateTo( resultsPageUrl );
        Thread.sleep(10000);
        WebElement resultCard = config.getDriver().findElement( By.tagName("yas-package-card"  ) );
        Assert.assertNotNull(resultCard);

    }
}
