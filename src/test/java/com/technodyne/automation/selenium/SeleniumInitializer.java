package com.technodyne.automation.selenium;

import com.technodyne.automation.selenium.config.SeleniumConfig;

public class SeleniumInitializer {

	private SeleniumConfig config;
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public SeleniumInitializer() {
		config = new SeleniumConfig();
	}

	public SeleniumInitializer(String url) {
		this.setUrl(url);
		config = new SeleniumConfig();
		config.getDriver().get(getUrl());
	}

	public void closeWindow() {
		this.config.getDriver().close();
	}

	public String getTitle() {
		return this.config.getDriver().getTitle();
	}

}
